import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {NgModule, Component} from '@angular/core';
import {FormsModule, ReactiveFormsModule, FormGroup, FormBuilder, Validators} from "@angular/forms";
import {BrowserModule} from '@angular/platform-browser';
import 'rxjs/add/operator/map';
import {BidService} from "./bid-service";
import {Bid} from "./bid";

@Component({
  selector: 'app',
  providers: [BidService],
  template: `<h3>Status: {{status}}</h3>
          <form #f="ngForm" [formGroup]="formModel"
            (ngSubmit)="onSubmit(f.value, f.valid)"
            novalidate>
              <input type="text" formControlName="username">
          </form>
          <button (click)="toggleBidSubscription(f.value, f.valid)">Toggle subscription to bid notifications</button><br>
  `
})

class BidComponent {

  newBid: Bid;
  subscribeToNotifications: boolean = false;
  status: string = "";

  formModel: FormGroup;


  constructor(private wsService: BidService, fb: FormBuilder) {

    this.formModel = fb.group({
      'username': ['', [Validators.required, Validators.minLength(5)]]
    });

    this.wsService.createObservableSocket("ws://localhost:8085")
      .map(res => JSON.parse(res))
      .subscribe(
        data => {
          this.newBid = data;
          // this.newBid.bidTime= data.bidTime || Date.parse(Date.now());
          console.log(this.newBid);
        },
        err => console.log(err),
        () => console.log('The bid stream is complete')
      );
  }

  toggleBidSubscription(foo? : any, valid? : boolean) {
    this.wsService.subscribeToBids(this.formModel.get('username').value);
  }
}

@NgModule({
  imports: [BrowserModule, FormsModule, ReactiveFormsModule],
  declarations: [BidComponent],
  bootstrap: [BidComponent]
})
class AppModule {
}

platformBrowserDynamic().bootstrapModule(AppModule);
