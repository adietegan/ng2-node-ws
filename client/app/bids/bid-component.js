"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var platform_browser_dynamic_1 = require("@angular/platform-browser-dynamic");
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var platform_browser_1 = require("@angular/platform-browser");
require("rxjs/add/operator/map");
var bid_service_1 = require("./bid-service");
var BidComponent = (function () {
    function BidComponent(wsService, fb) {
        var _this = this;
        this.wsService = wsService;
        this.subscribeToNotifications = false;
        this.status = "";
        this.formModel = fb.group({
            'username': ['', [forms_1.Validators.required, forms_1.Validators.minLength(5)]]
        });
        this.wsService.createObservableSocket("ws://localhost:8085")
            .map(function (res) { return JSON.parse(res); })
            .subscribe(function (data) {
            _this.newBid = data;
            // this.newBid.bidTime= data.bidTime || Date.parse(Date.now());
            console.log(_this.newBid);
        }, function (err) { return console.log(err); }, function () { return console.log('The bid stream is complete'); });
    }
    BidComponent.prototype.toggleBidSubscription = function (foo, valid) {
        this.wsService.subscribeToBids(this.formModel.get('username').value)
            .on('message', function () {
            console.
            ;
        });
    };
    return BidComponent;
}());
BidComponent = __decorate([
    core_1.Component({
        selector: 'app',
        providers: [bid_service_1.BidService],
        template: "<h3>Status: {{status}}</h3>\n          <form #f=\"ngForm\" [formGroup]=\"formModel\"\n            (ngSubmit)=\"onSubmit(f.value, f.valid)\"\n            novalidate>\n              <input type=\"text\" formControlName=\"username\">\n          </form>\n          <button (click)=\"toggleBidSubscription(f.value, f.valid)\">Toggle subscription to bid notifications</button><br>\n  "
    })
], BidComponent);
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [platform_browser_1.BrowserModule, forms_1.FormsModule, forms_1.ReactiveFormsModule],
        declarations: [BidComponent],
        bootstrap: [BidComponent]
    })
], AppModule);
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(AppModule);
