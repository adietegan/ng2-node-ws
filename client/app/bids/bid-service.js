"use strict";
var Rx_1 = require("rxjs/Rx");
var BidService = (function () {
    function BidService() {
    }
    BidService.prototype.createObservableSocket = function (url) {
        var _this = this;
        this.ws = new WebSocket(url);
        return new Rx_1.Observable(function (observer) {
            _this.ws.onmessage = function (event) {
                return observer.next(event.data);
            };
            _this.ws.onerror = function (event) { return observer.error(event); };
            _this.ws.onclose = function (event) { return observer.complete(); };
        });
    };
    BidService.prototype.subscribeToBids = function (input) {
        return this.ws.send(input);
    };
    return BidService;
}());
exports.BidService = BidService;
