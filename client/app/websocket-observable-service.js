"use strict";
var Rx_1 = require("rxjs/Rx");
var WebSocketService = (function () {
    function WebSocketService() {
    }
    WebSocketService.prototype.createObservableSocket = function (url) {
        var _this = this;
        this.ws = new WebSocket(url);
        return new Rx_1.Observable(function (observer) {
            _this.ws.onmessage = function (event) {
                return observer.next(event.data);
            };
            _this.ws.onerror = function (event) { return observer.error(event); };
            _this.ws.onclose = function (event) { return observer.complete(); };
        });
    };
    WebSocketService.prototype.sendMessage = function (message) {
        return this.ws.send(message);
    };
    return WebSocketService;
}());
exports.WebSocketService = WebSocketService;
