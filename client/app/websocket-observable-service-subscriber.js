"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var platform_browser_dynamic_1 = require("@angular/platform-browser-dynamic");
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var websocket_observable_service_1 = require("./websocket-observable-service");
var AppComponent = (function () {
    function AppComponent(wsService, fb) {
        var _this = this;
        this.wsService = wsService;
        this.wsService.createObservableSocket("ws://localhost:8085")
            .subscribe(function (data) {
            _this.messageFromServer = data;
        }, function (err) { return console.log(err); }, function () { return console.log('The observable stream is complete'); });
    }
    AppComponent.prototype.sendMessageToServer = function () {
        console.log("Sending message to WebSocket server");
        this.wsService.sendMessage("Hello from client");
    };
    AppComponent.prototype.onSubmit = function (value, valid) {
        console.log(value, valid);
    };
    return AppComponent;
}());
AppComponent = __decorate([
    core_1.Component({
        selector: 'app',
        providers: [websocket_observable_service_1.WebSocketService],
        template: "<h1>Angular subscriber to WebSocket service</h1>\n       {{messageFromServer}}<br>"
    })
], AppComponent);
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [platform_browser_1.BrowserModule],
        declarations: [AppComponent],
        bootstrap: [AppComponent]
    })
], AppModule);
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(AppModule);
